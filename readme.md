# Hi this is the coding challenge by Henrique Calixto de Oliveira

## This project is composed by the following:

   - DB folder containing “db.json” - the database file
   - robot.py - the Robot class code
   - app.py - the API content
   - sample.json - example request

## Before running please install the dependencies (tinydb and Flask),type the following on the terminal:
```
   pip3 install tinydb
```
```  
   pip3 install Flask
```

## To run, type the following on the terminal:
```
   python3 app.py
```
## To send a JSON files:

   - I recommend using Postman, it is an API client that facilitates
      sending JSON files to APIs.

      To download Postman chrome plugin:

      https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop?hl=en

   - If you are running the API locally you can send a POST
      HTTP request, with the JSON on the body, to:

      http://127.0.0.1:5000/postInstructions

      It will return a JSON response like this:
```
{
  "coords" : [1, 3],
  "patches" : 1
}
```

      Or it will return a error string
      
## To get the Database's information

   - If you are running the API locally you can send a POST
      HTTP request to:

      http://127.0.0.1:5000/dbInfo

      It will return a JSON with all the requests and responses

## To DELETE ALL the Database's information

   - If you are running the API locally you can send a POST
      HTTP request to:

      http://127.0.0.1:5000/dbDelete

      It will delete the all the requests and responses stored
      in the database, this is irreversible.

## To stop the app:

   - on the command line press ```Ctrl+c```

## Comments:

   - This application has simple requirements for the database,
      that is why I've chosen tinydb. It is simple and get the job done.

      https://pypi.org/project/tinydb/#description

   - For the web part I decided to go with flask since it is also
      simple and would cover what I need for this challenge.

      http://flask.pocoo.org/
