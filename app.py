#!flask/bin/python
from flask import Flask
from flask import request
from flask import jsonify
from tinydb import TinyDB, Query
from robot import Robot

app = Flask(__name__)

@app.route('/')
def index():
   return INSTRUCTIONS

@app.route('/postInstructions', methods=['POST'])
def post():
   if request.is_json:
      try:
         content = request.get_json()
         rob = Robot(content["roomSize"], content["coords"],\
                     content["patches"])

         rob.execute(content["instructions"])

         res = rob.getReport()
         #Save to Database
         db = TinyDB('DB/db.json')
         db.insert({"request":content,"response":res})

         return jsonify(res)
      except Exception as e:
         return str(e)
      
   else:
      return POSTJSONERROR

@app.route('/dbInfo', methods=['GET'])
def dbInfo():
   db = TinyDB('DB/db.json')
   return jsonify(db.all())

@app.route('/dbDelete', methods=['GET'])
def dbDelete():
   db = TinyDB('DB/db.json')
   db.purge()
   return jsonify(db.all())

#ERROR MSGS    
POSTJSONERROR = """Expected a JSON file like:
{
  "roomSize" : [5, 5],
  "coords" : [1, 2],
  "patches" : [
    [1, 0],
    [2, 2],
    [2, 3]
  ],
  "instructions" : "NNESEESWNWW"
}"""

INSTRUCTIONS = """
To send the JSON files:

   - I recommend using Postman, it is an API client that facilitates
      sending JSON files to APIs.

      To download Postman chrome plugin:

      https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop?hl=en

   - If you are running the API locally you can send a POST
      HTTP request, with the JSON on the body, to:

      http://127.0.0.1:5000/postInstructions

To get the Database's information

   - If you are running the API locally you can send a POST
      HTTP request to:

      http://127.0.0.1:5000/dbInfo

      It will return a JSON with all the requests and responses

To DELETE ALL the Database's information

   - If you are running the API locally you can send a POST
      HTTP request to:

      http://127.0.0.1:5000/dbDelete

      It will delete the all the requests and responses stored
      in the database, this is irreversible.
"""

if __name__ == '__main__':
   app.run(debug=True)