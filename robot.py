import re
import unittest

class Robot(object):
   """Robot class - move around and clean a virtual room"""
   def __init__(self, roomSize=[1,1], coords=[0,0], patches=[]):
      super(Robot, self).__init__()
      try:
         self.__roomX = 0
         self.__roomY = 0
         self.__robotX = 0
         self.__robotY = 0
         self.__dirt = [[]]
         self.__instructions = []
         self.__cleanCount = 0
         self.setRoomSize(roomSize)
         self.setRobotPosition(coords)
         self.setDirtMap(patches)
      except Exception as e:
         raise e
      

   #Methods
   def setRoomSize(self, roomSize):
      """Set the room dimensions - input: [x,Y]"""
      if self.__validateCoord(roomSize):
         self.__roomX = roomSize[0]
         self.__roomY = roomSize[1]
      else:
         raise ValueError("Bad input: roomSize has to be "+\
                     "a list [X,Y] of  positive coordinates "+\
                     ", not: {input}".format(input=repr(roomSize)))

   def setRobotPosition(self, coords):
      """Set robot's initial position - input: [x,Y]"""
      if self.__validatePosition(coords):
         self.__robotX = coords[0]
         self.__robotY = coords[1]
      else:
         raise ValueError("Bad input: coords has to be "+\
                     "a list [X,Y] of coordinates inside "+\
                     "the room, not: {input}".format(input=repr(coords)))

   def setDirtMap(self, patches):
      """Set a matrix with the dirt mapped on its XY position,
         1 means dirt 0 means clean. Here I decided to go with
         a Matrix because I assumed the room won't be too big
         so I decided to prioritize the read speed over the 
         memory size - input: [[x1,y1],[x2,y2],[x3,y3]]"""
      if self.__validatePositionList(patches):
         self.__dirt = [[0 for x in range(self.__roomX)] for y \
                        in range(self.__roomY)]
         for pos in patches:
            self.__dirt[pos[1]][pos[0]] = 1
      else:
         raise ValueError("Bad input: patches has to be "+\
                     "a list of valid coordinates (inside "+\
                     "the room), not: {input}".format(input=repr(patches)))

   def setInstructions(self, instructions):
      """Set robot's instructions - input: "NSWENSEW" """
      if self.__validateInstruction(instructions):
         self.__instructions = list(instructions)
      else:
         raise ValueError("Bad input: instructions has to be "+\
                     "a string with only 'NSWE' characters, "+\
                     "not: {input}".format(input=repr(instructions)))

   def getRoomSize(self):
      return [self.__roomX, self.__roomY]

   def getRobotPosition(self):
      return [self.__robotX, self.__robotY]

   def getDirtMap(self):
      return self.__dirt

   def getInstructions(self):
      return self.__instructions

   def getCleanCount(self):
      return self.__cleanCount

   def getReport(self):
      """Returns the report with its position and cleanCount"""
      return {"coords":self.getRobotPosition(), 
               "patches":self.getCleanCount()}

   def getNextPosition(self, instruction):
      """Returns the next position based on the instruction
         N|S|E|W, if the instruction or the next position are
         invalid returns the current position - input: "N|S|W|E" """
      posOrig = self.getRobotPosition()
      if self.__validateInstruction(instruction):
         pos = posOrig
         if instruction == "N" or instruction == "n":
            pos = [pos[0], pos[1]+1]
         elif instruction == "S" or instruction == "s":
            pos = [pos[0], pos[1]-1]
         elif instruction == "E" or instruction == "e":
            pos = [pos[0]+1, pos[1]]
         elif instruction == "W" or instruction == "w":
            pos = [pos[0]-1, pos[1]]

         if self.__validatePosition(pos):
            return pos
         else:
            return posOrig
      else:
         return posOrig

   def checkForDirt(self, pos):
      """Returns True if the position has dirt on it, if 
         it isn't a valid position returns false - input: [x,Y]"""
      if self.__validatePosition(pos):
         return bool(self.__dirt[pos[1]][pos[0]])
      else:
         return False

   def robotOverDirt(self):
      """ If the robot is over dirt it cleans it and return True"""
      return self.checkForDirt(self.getRobotPosition())

   def clean(self, pos):
      """ Cleans a position in the dirt map, if it is valid - input: [x,Y]"""
      if self.__validatePosition(pos):
         self.__dirt[pos[1]][pos[0]] = 0
      else:
         raise ValueError("Bad input: pos has to be "+\
                     "a list [X,Y] of coordinates inside "+\
                     "the room, not: {input}".format(input=repr(pos)))

   def move(self, instruction):
      """Moves the robot based on the instruction and if it 
         ends above a dirt spot cleans it and increse the 
         count - input: "N|S|W|E" """
      self.setRobotPosition(self.getNextPosition(instruction))
      if self.robotOverDirt():
         self.clean(self.getRobotPosition())
         self.__cleanCount += 1

   def execute(self, instructions):
      """Execute a set of instructions - input: "NSWENSEW" """
      self.setInstructions(instructions)
      for instruction in instructions:
         self.move(instruction)

   #Support functions
   def __validateCoord(self, coord):
      """Validate a coordinate, it must be a list containing
         two positivie integers - input: [x,Y]"""
      if type(coord) is list and len(coord) == 2 and\
         type(coord[0]) is int and type(coord[1]) is int and\
         coord[0] >= 0 and coord[1] >= 0:
         return True
      else:
         return False

   def __validatePosition(self, pos):
      """Validate if a position is valid - a valid coordinate and is 
         inside the room (less than the room's respective XY
         value) - input: [x,Y]"""
      if self.__validateCoord(pos) and pos[0] < self.__roomX and\
         pos[1] < self.__roomY:
         return True
      else:
         return False

   def __validatePositionList(self, posList):
      """Validate a list of positions, if some position is off
         the whole list is discarted - input: [[x1,y1],[x2,y2],[x3,y3]]"""
      if type(posList) is list:
         for pos in posList:
            if not self.__validatePosition(pos):
               return False
         return True
      else:
         return False

   def __validateInstruction(self, instr):
      """Validate a instruction, it must be a string containing
         only (Upper or lower) N or S or W or E - input: "NSWENSEW" """
      if type(instr) is str:
         return bool(re.match('^$|^[NnSsWwEe]+$', instr))
      else:
         return False

   def __printDirtMap(self):
      """Print the dirt matrix - for debugging"""
      for line in range(len(self.__dirt)-1,-1,-1):
         print(self.__dirt[line])

#TEST

class TestRobotMethods(unittest.TestCase):
   """Unit test for the Robot class"""

   def test_setRoomSize(self):
      rob = Robot()
      #Check it is working properly
      rob.setRoomSize([2,3])
      self.assertEqual(rob.getRoomSize(), [2,3])
      rob.setRoomSize([4,7])
      self.assertEqual(rob.getRoomSize(), [4,7])
      rob.setRoomSize([0,0])
      self.assertEqual(rob.getRoomSize(), [0,0])
      #Bad Input
      self.assertRaises(ValueError,rob.setRoomSize, [-1,0])
      self.assertRaises(ValueError,rob.setRoomSize, [1,-2])
      self.assertRaises(ValueError,rob.setRoomSize, [2,"0"])
      self.assertRaises(ValueError,rob.setRoomSize, {1,2})
      self.assertRaises(ValueError,rob.setRoomSize, [1])
      self.assertRaises(ValueError,rob.setRoomSize, [1,2,3])
      self.assertRaises(ValueError,rob.setRoomSize, "[1,1]")
      self.assertRaises(ValueError,rob.setRoomSize, [])

   def test_setRobotPosition(self):
      rob = Robot()
      rob.setRoomSize([5,5])
      #Check it is working properly
      rob.setRobotPosition([1,2])
      self.assertEqual(rob.getRobotPosition(), [1,2])
      rob.setRobotPosition([0,0])
      self.assertEqual(rob.getRobotPosition(), [0,0])
      rob.setRobotPosition([4,4])
      self.assertEqual(rob.getRobotPosition(), [4,4])
      #Bad Input
      self.assertRaises(ValueError,rob.setRobotPosition,[5,6])
      self.assertRaises(ValueError,rob.setRobotPosition,[3,7])
      self.assertRaises(ValueError,rob.setRobotPosition,[5,4])
      self.assertRaises(ValueError,rob.setRobotPosition,[-1,0])
      self.assertRaises(ValueError,rob.setRobotPosition,[1,-2])
      self.assertRaises(ValueError,rob.setRobotPosition,[2,"0"])
      self.assertRaises(ValueError,rob.setRobotPosition,{1,2})
      self.assertRaises(ValueError,rob.setRobotPosition,[1])
      self.assertRaises(ValueError,rob.setRobotPosition,[1,2,3])
      self.assertRaises(ValueError,rob.setRobotPosition,"[1,1]")
      self.assertRaises(ValueError,rob.setRobotPosition,[])

   def test_setDirtMap(self):
      rob = Robot()
      rob.setRoomSize([2,2])
      #Check it is working properly
      rob.setDirtMap([[1,1],[0,0]])
      self.assertEqual(rob.getDirtMap(), [[1,0],[0,1]])
      rob.setDirtMap([[1,0],[0,1],[0,0]])
      self.assertEqual(rob.getDirtMap(), [[1,1],[1,0]])
      rob.setDirtMap([[1,0]])
      self.assertEqual(rob.getDirtMap(), [[0,1],[0,0]])
      #Bad Input
      self.assertRaises(ValueError,rob.setDirtMap,[1,0])
      self.assertRaises(ValueError,rob.setDirtMap,[[1,0],[2,1]])
      self.assertRaises(ValueError,rob.setDirtMap,[[1,0],[-1,1]])
      self.assertRaises(ValueError,rob.setDirtMap,[[1,"0"],[1,1]])
      self.assertRaises(ValueError,rob.setDirtMap,"[[1,0],[1,1]]")
      self.assertRaises(ValueError,rob.setDirtMap,[[1,0],[1,1,1]])
      self.assertRaises(ValueError,rob.setDirtMap,[[]])

   def test_setInstructions(self):
      rob = Robot()
      rob.setRoomSize([2,2])
      #Check it is working properly
      rob.setInstructions("NSEW")
      self.assertEqual(rob.getInstructions(), ["N","S","E","W"])
      rob.setInstructions("")
      self.assertEqual(rob.getInstructions(), [])
      rob.setInstructions("NsE")
      self.assertEqual(rob.getInstructions(), ["N","s","E"])
      #Bad Input
      self.assertRaises(ValueError,rob.setInstructions,["N","S"])
      self.assertRaises(ValueError,rob.setInstructions,"NSEWA")
      self.assertRaises(ValueError,rob.setInstructions,"NSEW1")
      self.assertRaises(ValueError,rob.setInstructions,"NSEW1")

   def test_getNextPosition(self):
      rob = Robot()
      rob.setRoomSize([2,2])
      #Check it is working properly
      rob.setRobotPosition([0,0])
      self.assertEqual(rob.getNextPosition("N"),[0,1])
      self.assertEqual(rob.getNextPosition("E"),[1,0])
      self.assertEqual(rob.getNextPosition("S"),[0,0])
      self.assertEqual(rob.getNextPosition("W"),[0,0])
      rob.setRobotPosition([1,1])
      self.assertEqual(rob.getNextPosition("S"),[1,0])
      self.assertEqual(rob.getNextPosition("W"),[0,1])
      self.assertEqual(rob.getNextPosition("N"),[1,1])
      self.assertEqual(rob.getNextPosition("E"),[1,1])
      #Ignoring bad inputs
      self.assertEqual(rob.getNextPosition("NS"),[1,1])
      self.assertEqual(rob.getNextPosition("A"),[1,1])
      self.assertEqual(rob.getNextPosition("2"),[1,1])
      self.assertEqual(rob.getNextPosition(["E"]),[1,1])
      self.assertEqual(rob.getNextPosition(3),[1,1])

   def test_checkForDirt(self):
      rob = Robot()
      rob.setRoomSize([2,2])
      rob.setDirtMap([[1,1],[0,0]])
      #Check it is working properly
      self.assertEqual(rob.checkForDirt([0,0]),True)
      self.assertEqual(rob.checkForDirt([0,1]),False)
      self.assertEqual(rob.checkForDirt([1,0]),False)
      self.assertEqual(rob.checkForDirt([1,1]),True)
      #Ignoring bad input
      self.assertEqual(rob.checkForDirt([1,3]),False)
      self.assertEqual(rob.checkForDirt([5,5]),False)
      self.assertEqual(rob.checkForDirt("A"),False)
      self.assertEqual(rob.checkForDirt([1]),False)
      self.assertEqual(rob.checkForDirt([[1,0]]),False)

   def test_robotOverDirt(self):
      rob = Robot()
      rob.setRoomSize([2,2])
      rob.setDirtMap([[1,1],[0,0]])
      #Check it is working properly
      rob.setRobotPosition([0,0])
      self.assertEqual(rob.robotOverDirt(),True)
      rob.setRobotPosition([0,1])
      self.assertEqual(rob.robotOverDirt(),False)
      rob.setRobotPosition([1,0])
      self.assertEqual(rob.robotOverDirt(),False)
      rob.setRobotPosition([1,1])
      self.assertEqual(rob.robotOverDirt(),True)

   def test_clean(self):
      rob = Robot()
      rob.setRoomSize([2,2])
      rob.setDirtMap([[1,1],[0,0]])
      #Check it is working properly
      self.assertEqual(rob.getDirtMap(), [[1,0],[0,1]])
      rob.clean([0,1])
      self.assertEqual(rob.getDirtMap(), [[1,0],[0,1]])
      rob.clean([0,0])
      self.assertEqual(rob.getDirtMap(), [[0,0],[0,1]])
      rob.clean([1,0])
      self.assertEqual(rob.getDirtMap(), [[0,0],[0,1]])
      rob.clean([1,1])
      self.assertEqual(rob.getDirtMap(), [[0,0],[0,0]])

   def test_move(self):
      rob = Robot()
      rob.setRoomSize([2,2])
      rob.setRobotPosition([0,0])
      rob.setDirtMap([[1,1],[0,0]])
      #Check it is working properly
      self.assertEqual(rob.getCleanCount(), 0)
      rob.move("N")
      self.assertEqual(rob.getCleanCount(), 0)
      self.assertEqual(rob.getRobotPosition(), [0,1])
      rob.move("E")
      self.assertEqual(rob.getCleanCount(), 1)
      self.assertEqual(rob.getRobotPosition(), [1,1])
      rob.move("S")
      self.assertEqual(rob.getCleanCount(), 1)
      self.assertEqual(rob.getRobotPosition(), [1,0])
      rob.move("S")
      self.assertEqual(rob.getCleanCount(), 1)
      self.assertEqual(rob.getRobotPosition(), [1,0])
      rob.move("W")
      self.assertEqual(rob.getCleanCount(), 2)
      self.assertEqual(rob.getRobotPosition(), [0,0])
      rob.move("W")
      self.assertEqual(rob.getCleanCount(), 2)
      self.assertEqual(rob.getRobotPosition(), [0,0])
      #Dealing with bad input
      rob.move("A")
      self.assertEqual(rob.getCleanCount(), 2)
      self.assertEqual(rob.getRobotPosition(), [0,0])
      rob.move(["W"])
      self.assertEqual(rob.getCleanCount(), 2)
      self.assertEqual(rob.getRobotPosition(), [0,0])
      rob.move(1)
      self.assertEqual(rob.getCleanCount(), 2)
      self.assertEqual(rob.getRobotPosition(), [0,0])
      
   def test_execute(self):
      rob = Robot()
      rob.setRoomSize([2,2])
      rob.setRobotPosition([0,0])
      rob.setDirtMap([[1,1],[0,0]])
      #Check it is working properly
      rob.execute("NNNN")
      self.assertEqual(rob.getCleanCount(), 0)
      self.assertEqual(rob.getRobotPosition(), [0,1])
      rob.execute("NEWNE")
      self.assertEqual(rob.getCleanCount(), 1)
      self.assertEqual(rob.getRobotPosition(), [1,1])
      rob.execute("NEWNESSSNSW")
      self.assertEqual(rob.getCleanCount(), 2)
      self.assertEqual(rob.getRobotPosition(), [0,0])
      #Bad Input
      self.assertRaises(ValueError,rob.execute,"NEWNESSSN1W")
      self.assertRaises(ValueError,rob.execute,"NEWNESSSNAW")
      self.assertRaises(ValueError,rob.execute,["NS","E"])
      self.assertRaises(ValueError,rob.execute,["N","E","W"])

   def test_getReport(self):
      rob = Robot()
      rob.setRoomSize([2,2])
      rob.setRobotPosition([0,0])
      rob.setDirtMap([[1,1],[0,0]])
      #Check it is working properly
      rob.execute("NNNN")
      rep = rob.getReport()
      self.assertEqual(rep["patches"], 0)
      self.assertEqual(rep["coords"], [0,1])
      rob.execute("NEWNE")
      rep = rob.getReport()
      self.assertEqual(rep["patches"], 1)
      self.assertEqual(rep["coords"], [1,1])
      rob.execute("NEWNESSSNSW")
      rep = rob.getReport()
      self.assertEqual(rep["patches"], 2)
      self.assertEqual(rep["coords"], [0,0])

if __name__ == '__main__':
    unittest.main()